
# Lost In Translation
Lost in Translation is an apllication where you can translate simple text to sign language.


[The application can be found here](https://nameless-oasis-48353.herokuapp.com)

## Contributors:
[Karianne Tesaker](https://gitlab.com/kariannet) and  [Esben Bjarnason](https://gitlab.com/EsbenLB)

## Description:
#### LoginPage
TextBox: Type in your user name.
Button arrow: Log in with user name.
#### TranslatePage
TextBox: Type in words to translate.
Button arrow: Translate word in TextBox to sign pictures.
#### ProfilePage
Display last 10 translations.
Button delete: Hide the last 10 translations.
Button Logout: Logout user


## Installation
Run following commands to install Node (required)

```bash
npm install
```
## Usage
In project folder:
```bash
npm start
```
## Project status
...
Done!

## Component tree

[ComponetTree.pdf](https://gitlab.com/kariannet/assignment-3-lost-in-translation/-/blob/main/README_figures/ComponentTree.pdf)
