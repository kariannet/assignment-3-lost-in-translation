const PATH="https://noroff-assignment-api-karianne.herokuapp.com"
const API_KEY = "L9M22VKQ09XY6B4NPMRL4VWOBX2QN9SVQVD6O87H3LZR334OFTNV3AT43WR6IXK9";

// Return list of users
export const LoginAPI = {
    
    login(credentials){
        return fetch(PATH+`/translations?username=${credentials.username}`)
        
        .then(response => response.json())
    }
}
// Create new user
export const CreateNewUserAPI = {
    newUser(credentials){
        return fetch(`${PATH}/translations`, {
            method: 'POST',
            headers: {
              'X-API-Key': API_KEY,
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({ 
                username: credentials.username, 
                translations: []
            })
        })
        .then(response => {
          if (!response.ok) {
            throw new Error('Could not create new user')
          }
          return response.json()
        })
    }
}