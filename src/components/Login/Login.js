import {useState} from 'react'
import {useDispatch, useSelector } from 'react-redux'
import AppContainer from "../../hoc/AppContainer"
import { loginAttemptAction } from '../../store/actions/loginActions'

import {Navigate}  from 'react-router-dom'
import "./Login.css"

function Login(){

const dispatch = useDispatch()
const { loginError, loginAttempting } = useSelector(state => state.loginReducer )

const { loggedIn} = useSelector(state => state.sessionReducer )

const [ credentials, setCredentials ] = useState({
    username: '',
    password: ''
})

const onInputChange = event => {
    setCredentials({
        ...credentials,
        [event.target.id]: event.target.value
    })
}
const onFormSubmit = async event => {
    event.preventDefault()
    // Try to login
    dispatch(loginAttemptAction( credentials))
}

    return(

        <>
            {/* Check if logged in = go to translation page, else show login page */}
            { loggedIn && <Navigate  to="/translation">Hei</Navigate> }
            { !loggedIn && 
                <AppContainer>
                    <form className="mt-3" onSubmit={ onFormSubmit }>
                    

                        <div className="mb-3">
                            <label htmlFor="username" className="form-label">Get started
                            </label>
                            <span className="input-group-text" id="addon-wrapping">
                                <input id="username" type="text" placeholder="What's your name?" 
                                className="form-control" onChange={ onInputChange}></input>
                                <button type="submit"  className="btn btn-secondary" >
                                    <span className='material-icons'>arrow_forward</span>
                                </button>    
                            
                            </span>
                        </div>

                    </form>
                    { loginAttempting &&
                        <p>Trying to login...</p>
                    }
                    { loginError &&
                        <div className="alert alert-danger" role="alert">
                            <h4>Unsuccessful</h4>
                            <p className="mb-0">{ loginError }</p>
                        </div>
                    }
                </AppContainer>
            }
            
        </>

        
    )
}

export default Login