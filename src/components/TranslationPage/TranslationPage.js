import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Navigate, NavLink } from "react-router-dom";
import AppContainer from "../../hoc/AppContainer"
import { translationAttemptAction } from "../../store/actions/translationActions";
import "./TranslationPage.css"

function TranslationPage(){
    
    const dispatch = useDispatch();
    const { loggedIn } = useSelector(state => state.sessionReducer);
    let displayTranslations = [];

    const getTranslations = () => {
        const storedSession = localStorage.getItem('litr-profile')
        if (!storedSession) {
            return [[], null];
        }
        const session = JSON.parse(storedSession)


        return [session.translations, session.id];
    }

    let [ translations, userId ] = getTranslations();

    const [ translation, setTranslation ] = useState({
        translationText: "",
        isDeleted: false
    })

    const [showImages, setImages] = useState({
        showImages: displayTranslations,
        display: false
    })

    /**
     * Checks if a character is a letter.
     * @param {string} inputLetter - a single character
     * @returns boolean - true if the input character is a letter
     */
    const isLetter = (inputLetter) => {
        if (inputLetter.match(/[A-Za-z]/)) return true; 
        else return false;
    }
    
    const getTranslationImage = (letter, i) => {
        if (isLetter(translation.translationText[i])) {
            const imagePath = `/images/${letter.toLowerCase()}.png`;
            displayTranslations.push(<img src={imagePath} key={i} className="image" alt={imagePath}/>)
        }
        else {
            displayTranslations.push(<p key={i}>{letter}</p>)
        }
    }

    const onInputChange = event => {
        setTranslation({
            ...translation,
            translationText: event.target.value
        })
    }

    const onTranslationsFormSubmit = event => {
        event.preventDefault();

        displayTranslations = [];
        for (let i = 0; i < translation.translationText.length; i++) {
            getTranslationImage(translation.translationText[i], i);
        }
        setImages({
            ...showImages,
            showImages: displayTranslations,
            display: true
        })
        translations.push(translation)
        dispatch(translationAttemptAction(userId, translations));
    }

    return(
        <>
            { !loggedIn && <Navigate to="/" />}
            { loggedIn &&
                <AppContainer>
                    <form className="mt-5 mb-3" onSubmit={ onTranslationsFormSubmit } >
                        <div className="input-group flex-nowrap">
                            <span className="input-group-text" id="addon-wrapping">
                                <span className='material-icons'>keyboard</span>
                            </span>
                            <input id="translationText" type="text" 
                            className="form-control" 
                            placeholder="Please enter the text you want to translate" 
                            aria-label="Translate"
                            onChange={onInputChange} />
                            <button 
                            className="btn btn-secondary" 
                            type="submit" 
                            id="translate-button">
                                <span className='material-icons'>arrow_forward</span>
                            </button>
                        </div>
                    </form>

                    <div className="container mb-3">
                        <div className="card border-3">
                            <div id="cardBody" className="card-body Card-body">
                                { showImages.display &&
                                    showImages.showImages
                                }
                                
                            </div>
                            <div className="card-footer Card-footer">
                                Translation
                            </div>
                        </div>

                    </div>

                    <div className="mb-3 float-end">
                        <a  
                            className="btn btn-primary" 
                            href="/profile"
                            role="button"> View Profile
                        </a>
                    </div>

                </AppContainer>
            }
        </>
    )
}




export default TranslationPage