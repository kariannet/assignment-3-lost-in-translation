import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Navigate } from "react-router-dom";
import AppContainer from "../../hoc/AppContainer"
import { translationAttemptAction } from "../../store/actions/translationActions";

function ProfilePage(){
    
    const dispatch = useDispatch();
    const { loggedIn } = useSelector (state => state.sessionReducer);

    const getTranslations = () => {
        const storedSession = localStorage.getItem('litr-profile')
        if (!storedSession) {
            return [[], null];
        }
        const session = JSON.parse(storedSession)


        return [session.translations, session.id];
    }

    let [ translations, userId ] = getTranslations();

    // const checkIfDele

    const displayTranslations = (translations) => {
        let resultDisplayTranslations = []
        let count = 0;
        for (let i = translations.length-1; i >= 0; i--) {
            if (!translations[i].isDeleted) {
                resultDisplayTranslations.push(
                    <li key={i} className="list-group-item">{translations[i].translationText}</li>
                    )
                count ++;
                if (count === 10) {
                    break;
                }
            }
        }
        return resultDisplayTranslations;
    }

    const onSignOut = event => {
        localStorage.clear();
        
    }

    const [showTranslations, setShowTranslations] = useState({
        showTranslations: displayTranslations(translations)
    })

    const onDeleteTranslations = event => {
        [ translations, userId ] = getTranslations();
        let count = 0;
        for (let i = translations.length-1; i >= 0; i--) {
            console.log(translations[i].isDeleted)
            if (!translations[i].isDeleted) {
                translations[i].isDeleted = true;
                count ++;
                if (count === 10) {
                    break;
                }
            }
        }
        setShowTranslations({
            ...showTranslations,
            showTranslations: displayTranslations(translations)
        })
        dispatch(translationAttemptAction(userId, translations));
    }

    return(
        <>
            { !loggedIn && <Navigate to="/" />}
            { loggedIn &&
                <AppContainer>

                    <div className="container m-3">
                        <div className="card border-3">
                            <div className="card-header">
                                Your last ten translations:
                            </div>
                            <div id="cardBody" className="card-body">
                                <ul className="list-group list-group-flush">
                                    {showTranslations.showTranslations}
                                </ul>
                            </div>
                        </div>

                    </div>

                    <div className="mb-3 float-end">
                        <button  
                            className="btn btn-danger mx-3" 
                            onClick={onDeleteTranslations}> Delete Translations
                        </button>
                        <a  
                            className="btn btn-primary" 
                            onClick={onSignOut}
                            href="/"
                            role="button"> Sign Out
                        </a>
                    </div>
                </AppContainer>
            }
        </>
    )
}

export default ProfilePage