const PATH="https://noroff-assignment-api-karianne.herokuapp.com/translations"
const API_KEY = "L9M22VKQ09XY6B4NPMRL4VWOBX2QN9SVQVD6O87H3LZR334OFTNV3AT43WR6IXK9";

export const updateTranslationsAPI = {
    updateTranslations(userID, translations) {

        return fetch(`${PATH}/${userID}`, {
            method: 'PATCH',
            headers: {
                'X-API-KEY': API_KEY,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                translations: translations
            })
        })
        .then(response => {
            if (!response.ok) {
              throw new Error('Could not update translations history')
            }
            return response.json()
          })
    }
}