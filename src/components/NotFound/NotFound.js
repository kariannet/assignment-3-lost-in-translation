import {Link} from 'react-router-dom';
// When typing wrong web adr. Display this page with link back to start/Login
const NotFound = () => {
    return (
        <main>
            <h4>Hey, you seem lost.</h4>
            <p>This page does not exist</p>
            <Link to="/">Take me home</Link>
        </main>
    )
}

export default NotFound;