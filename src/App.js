import './App.css';
import Login from './components/Login/Login'
import NotFound from './components/NotFound/NotFound'
import ProfilePage from './components/ProfilePage/ProfilePage'
import TranslationPage from './components/TranslationPage/TranslationPage'

import {
  BrowserRouter,
  Routes,
  Route,
  NavLink,
  Redirect
} from 'react-router-dom';

function App() { 
  return (
    <BrowserRouter>
      <div className="App">
        <header className='App-header'><h1>Lost in Translation</h1>
        </header>
          {/* Here goes the routes */}
          <Routes>
            <Route path="/" element={ < Login />} />
            <Route path="/profile" element={ < ProfilePage />} />
            <Route path="/translation" element={ < TranslationPage />} />
            <Route path="*" element={ < NotFound />} />
          </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
