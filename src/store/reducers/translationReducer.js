import { ACTION_TRANSLATION_ATTEMPTING, ACTION_TRANSLATION_ERROR, ACTION_TRANSLATION_SUCCESS } from "../actions/translationActions"

const initialState = {
    saveTranslationAttempting: false,
    saveTranslationError: ""
}

export const translationReducer = (state=initialState, action) => {
    switch (action.type) {
        case ACTION_TRANSLATION_ATTEMPTING:
            return {
                ...state,
                saveTranslationAttempting: true,
                saveTranslationError: ""
            }
        case ACTION_TRANSLATION_SUCCESS:
            return {
                ...initialState
            }
        case ACTION_TRANSLATION_ERROR:
            return {
                ...state,
                saveTranslationAttempting: false,
                saveTranslationError: action.payload
            }
        default:
            return state
    }
}