import { ACTION_LOGIN_ATTEMPTING, ACTION_LOGIN_SUCCESS, ACTION_LOGIN_ERROR,
    // ACTION_LOGIN_SESSION_SET, ACTION_LOGIN_SESSION_INIT
} from "../actions/loginActions"
const initialState = {
    loginAttempting: false,
    loginError: '',
}


export const loginReducer = (state = initialState, action) => {
    //console.log(action)
    switch (action.type) {
        // '[login] ATTEMPT'
        case ACTION_LOGIN_ATTEMPTING:
            return {
                // ...state = We have old state
                ...state,
                loginAttempting: true,
                loginError: '' // ''
            }
        case ACTION_LOGIN_SUCCESS:
            return {
                // return state. nothing more
                ...initialState
            }
            // Update. something went wrong
        case ACTION_LOGIN_ERROR:
            return {
                ...state,
                loginAttempting: false,
                loginError: action.payload
            }
        default:
            return state
    }
}

