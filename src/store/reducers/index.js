import { combineReducers } from 'redux';
import { translationReducer } from './translationReducer';
import { loginReducer } from "./loginReducer";
import { sessionReducer } from "./sessionReducer";
// Bundle all login files for store
const appReducer = combineReducers({
    translationReducer,
    loginReducer,
    sessionReducer
}) 
export default appReducer
