export const ACTION_TRANSLATION_ATTEMPTING = '{translation} ATTEMPT';
export const ACTION_TRANSLATION_SUCCESS = '{translation} SUCCESS';
export const ACTION_TRANSLATION_ERROR = '{translation} ERROR';

export const translationAttemptAction = (userID, translateText) => ({
    type: ACTION_TRANSLATION_ATTEMPTING,
    payload: { userID, translateText }
});

export const translationSuccessAction = updatedProfile => ({
    type: ACTION_TRANSLATION_SUCCESS,
    payload: updatedProfile
});

export const translationErrorAction = error => ({
    type: ACTION_TRANSLATION_ERROR,
    payload: error
});