
import { LoginAPI, CreateNewUserAPI } from "../../components/Login/LoginAPI"
import { ACTION_LOGIN_ATTEMPTING, ACTION_LOGIN_SUCCESS, 
    loginErrorAction, loginSuccessAction } from "../actions/loginActions"
import { sessionSetAction } from "../actions/sessionActions"

export const loginMiddleware = ({ dispatch }) => next => action => {

    next(action)
    
    if (action.type === ACTION_LOGIN_ATTEMPTING) {

        LoginAPI.login(action.payload)
            .then(response => {
                // if one user. continue
                if(response.length === 1){
                    dispatch( loginSuccessAction(response[0]))
                }
                // If noe user. Create user
                else if(response.length === 0){
                    CreateNewUserAPI.newUser(action.payload)
                    .then(newUser =>{
                        // Login Success
                        dispatch( loginSuccessAction(newUser))
                    })
                }else{
                    // To many user with same name. error
                    dispatch(loginErrorAction("no or to many user account with that name"))
                }
                
        }).catch(error => {
            dispatch(loginErrorAction(error.message))
        })
        
    }
    // Store set state
    if (action.type === ACTION_LOGIN_SUCCESS) {
        dispatch(sessionSetAction(action.payload))
    }
}
