import { updateTranslationsAPI } from "../../components/api/TranslationsAPI";
import { ACTION_TRANSLATION_ATTEMPTING, ACTION_TRANSLATION_SUCCESS, translationErrorAction, translationSuccessAction } from "../actions/translationActions";

export const translationMiddleware = ({ dispatch }) => next => action => {
    next(action);

    if (action.type === ACTION_TRANSLATION_ATTEMPTING) {
        updateTranslationsAPI.updateTranslations(action.payload.userID, action.payload.translateText)
        .then(profile => {
            dispatch(translationSuccessAction(profile))
        })
        .catch(error => {
            dispatch(translationErrorAction(error.message))
        })
    }
    
    if (action.type === ACTION_TRANSLATION_SUCCESS) {
        localStorage.setItem('litr-profile', JSON.stringify(action.payload))
    }
}