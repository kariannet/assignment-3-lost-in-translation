import { applyMiddleware } from "redux";
import { translationMiddleware } from "./translationMiddleware";
import { loginMiddleware } from "./loginMiddleware";
import { sessionMiddleware } from "./sessionMiddleware";
// Bundle all Middleware files
export default applyMiddleware(
    translationMiddleware,
    loginMiddleware,
    sessionMiddleware
)