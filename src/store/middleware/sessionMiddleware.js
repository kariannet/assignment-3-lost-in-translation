
import { ACTION_SESSION_INIT, ACTION_SESSION_SET, sessionSetAction } from "../actions/sessionActions"

export const sessionMiddleware = ({ dispatch}) => next => action => {
    next(action)

    if(action.type === ACTION_SESSION_INIT){
        // get localStorage
        const storedSession = localStorage.getItem('litr-profile')
        if(!storedSession){
            return
        }
        const session = JSON.parse(storedSession)
        // Set state
        dispatch(sessionSetAction(session))
    }

    if(action.type === ACTION_SESSION_SET){
        // Save to LocalStorage
        localStorage.setItem('litr-profile', JSON.stringify(action.payload))
        
    }
}