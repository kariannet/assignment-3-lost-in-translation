import { createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import appReducer from "./reducers"; //because we've called the files "index.js", we don't have to
import middleware from "./middleware"; // specify the import in more detail
// Store with all bundles
export default createStore(
    appReducer,
    composeWithDevTools(middleware),

    
)